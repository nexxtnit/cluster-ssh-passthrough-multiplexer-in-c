#include <arpa/inet.h>  // defines in_addr structure
#include <netinet/in.h> //contains constants and structures needed for internet domain addresses
#include <pthread.h>    // contains threads
#include <signal.h>
#include <stdio.h>      // standard input and output library
#include <stdlib.h>     // this includes functions regarding memory allocation
#include <string.h>     // contains string functions
#include <sys/select.h> //for select statement
#include <sys/socket.h> // for socket creation
#include <sys/wait.h>
#include <unistd.h> //contains various constants

#include <sys/time.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

#define PORTREC 22280
#define BUFF 2048
#define PI_PORT 22 // pi ssh port
#define PORTSEND 44480

static const int MAXPENDING = 5; // Maximum outstanding connection requests
struct clnt_to_pi_cnct
{
  int clnt_sd;                 // client socketDescriptor
  struct sockaddr_in clntAddr; // client address
  int pi_sd;                   // pi fd
  struct sockaddr_in piAddr;   /// pi address
};

struct rtt_vals
{
  //all times are in usec
  uint32_t clientSend_sec, clientSend_usec, raspiRec_sec,
      raspiRec_usec, raspiSend_sec, raspiSend_usec,
      clientRec_sec, clientRec_usec;
};
// end programm with console message
void dieWithSysMessage(const char *msg);
// create listen socket on portrec
int createServerSocket_TCP(int portrec);
// creat send/receive port for UDP
int createServerSocket_UDP(char *sendAddress);
// find best pi on cluster
char *getBestPi();
//meassure rtt on pi and client
void roundTripTimePi(char *ipAddress);
// return connection struct of client and pi
struct clnt_to_pi_cnct acceptConnection(int *serverSocket);
// send and receive client<->pi
void sendReceiveThread(struct clnt_to_pi_cnct passedConnection);
// cleanup zombie process
void processCleanup(unsigned int *childProcCount);

int main(int argc, char const *argv[])
{
  int portRec = PORTREC;
  if (argc == 2)
    portRec = atoi(argv[1]);
  printf("server Started\n");

  int serverSocket, len;
  struct clnt_to_pi_cnct clntPiConnection;
  unsigned int childProcCount = 0; // Number of child processes

  // create socket on server
  serverSocket = createServerSocket_TCP(portRec);
  printf("accepting connection\n");

  do
  {
    clntPiConnection = // accept connection on server socket
        acceptConnection(&serverSocket);
    pid_t processID = fork();
    if (processID < 0) // error fork()
      dieWithSysMessage("fork() failed");
    else if (processID == 0)
    {                      // this is child process
      close(serverSocket); // Child closes parent socket
      sendReceiveThread(   // create thread for forwarding data client<->pi
          clntPiConnection);
    }
    // this is parent process
    close(clntPiConnection.clnt_sd);
    close(clntPiConnection.pi_sd);
    processCleanup(&childProcCount); // check on zombie processes
    childProcCount++;
    printf("child nr %d created\n", childProcCount);
    if (childProcCount > 5)
      printf("maximum of 5 similar connections reached \n");

  } while (1);
  close(serverSocket);
  // never reached
  return 0;
}

void dieWithSysMessage(const char *msg)
{
  perror(msg);
  exit(0);
}

int createServerSocket_TCP(int portRec)
{
  struct sockaddr_in servAddr, clntAddr;
  int receiveSock;
  if ((receiveSock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    dieWithSysMessage("socket creation failed");
  printf("socket created\n");

  // construct local address structure
  memset(&servAddr, 0, sizeof(servAddr));       // Zero out structure
  servAddr.sin_family = AF_INET;                // IPv4 address family
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY); // Any incoming interface
  servAddr.sin_port = htons(portRec);           // Local port

  // bind to the local address
  if (bind(receiveSock, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
    dieWithSysMessage("bind() failed");

  // mark the socket so it will listen for incoming connections
  if (listen(receiveSock, MAXPENDING) < 0)
    dieWithSysMessage("listen() failed");
  printf("server listening on port: %d \n", portRec);
  return receiveSock;
}

int createServerSocket_UDP(char *sendAddress)
{

  int socketdescriptor;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  socklen_t addr_size;

  if ((socketdescriptor = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    dieWithSysMessage("udp_socket creation failed");

  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORTSEND);
  serverAddr.sin_addr.s_addr = inet_addr(sendAddress);
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);
  addr_size = sizeof(serverAddr);
  if (bind(socketdescriptor, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
    dieWithSysMessage("udp_bind failed");

  return socketdescriptor;
}

char *getBestPi()
{
  static char piAddress[15];
  char line[15];

  // get ip of all pi on cluster
  FILE *pipe = popen("cat /etc/hosts | awk '{print $1}' |grep '172.16.2'", "r");
  if (pipe == NULL)
    dieWithSysMessage("getBestPi() error on pipe");
  int usersTemp =
      2; // value for how many users are still acceptable to connect
  while (fgets(line, 15, pipe) != NULL)
  {                     // iterate through pi available
    strtok(line, "\n"); // remove \n of line
    char command[128];
    strcpy(command, "ssh brodmann@");
    strcat(command, line);
    strcat(command, " who | awk '{print $1}' |sort -u | wc -l");
    FILE *buffer = popen(command, "r"); // get active users
    char str[2];
    fgets(str, 2, buffer);
    char *end;
    int users = strtol(str, &end, 10);
    printf("%s having %d users\n", line, users);
    if (users == 0)
    {
      strcpy(piAddress, line);
      break;
    }
    if (users <= usersTemp)
    {
      usersTemp = users;
      strcpy(piAddress, line);
    }
  }
  pclose(pipe); //close pipe
  printf("%s chosen\n", piAddress);
  return piAddress;
}

double printTimeDifference(uint32_t time1_sec, uint32_t time1_usec, uint32_t time0_sec, uint32_t time0_usec)
{
  if (difftime(time1_sec, time0_sec) == 0)
  {
    double timediff_usec = difftime(time1_usec, time0_usec);
    if (timediff_usec < 0)
    {
      printf("ERR: Timedifference negative, receive-timestamp before send-timestamp\n");
      return -1;
    }
    else
    {
      return (timediff_usec * 0.000001);
    }
  }
  else if (difftime(time1_sec, time0_sec) > 0)
  {
    double timediff_sec = difftime(time1_sec, time0_sec);
    timediff_sec += (difftime(time1_usec, time0_usec) * 0.000001);
  }
  else
  {
    printf("ERR: Timedifference negative, receive-timestamp before send-timestamp\n");
    return -1;
  }
}

void roundTripTimePi(char *piAddress)
{
  struct rtt_vals rtt;
  struct timeval tv, tv_ioctl;
  uint32_t clientReady_sec, clientReady_usec;

  //create UDP socket
  int udpSocket_d;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  socklen_t addr_size;
  if ((udpSocket_d = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    dieWithSysMessage("udp_socket creation failed");
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORTSEND);
  serverAddr.sin_addr.s_addr = inet_addr(piAddress);
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);
  addr_size = sizeof(serverAddr);

  char command[128];

  strcpy(command, "ssh brodmann@");
  strcat(command, piAddress);
  //execute command on raspberry in background with nohup
  strcat(command, " nohup /home/brodmann/verteilteSysteme/Aufgabe4_rtt/a.out 1> /dev/null 2> /dev/null &");
  FILE *pipe = popen(command, "r"); //start server on raspberry
  if (pipe == NULL)
    dieWithSysMessage("ssh on raspberry failed");
  sleep(1);//wait for 1 seconde
  gettimeofday(&tv, NULL);        //get time right before client sends
  rtt.clientSend_sec = tv.tv_sec; //time client sends
  rtt.clientSend_usec = tv.tv_usec;
  if (sendto(udpSocket_d, &rtt, sizeof(rtt), 0, (struct sockaddr *)&serverAddr,
             addr_size) < 0)
    dieWithSysMessage("udp_send failed");
  if (recv(udpSocket_d, &rtt, sizeof(rtt), 0) < 0)
    perror("recv failed");
  if (ioctl(udpSocket_d, SIOCGSTAMP, &tv_ioctl) < 0)
    perror("ioctl failed");

  gettimeofday(&tv, NULL); //get time client is ready
  rtt.clientRec_sec = tv_ioctl.tv_sec;
  rtt.clientRec_usec = tv_ioctl.tv_usec;
  clientReady_sec = tv.tv_sec;
  clientReady_usec = tv.tv_usec;
  double roundTripTime = printTimeDifference(rtt.clientRec_sec,
                                             rtt.clientRec_usec,
                                             rtt.clientSend_sec,
                                             rtt.clientSend_usec);
  if (roundTripTime >= 0)
    printf("round trip time: %f s \n", roundTripTime);
  double networkToProgTime = printTimeDifference(clientReady_sec,
                                                 clientReady_usec,
                                                 rtt.clientRec_sec,
                                                 rtt.clientRec_usec);
  if (networkToProgTime >= 0 && networkToProgTime <= 10000)
    printf("transfer time networkadapter -> client: %f s \n", networkToProgTime);
  close(udpSocket_d);

  strcpy(command, "ssh brodmann@");
  strcat(command, piAddress);
  strcat(command, " killall a.out > /dev/null");
  pipe = popen(command, "r");
  pclose(pipe);
}

struct clnt_to_pi_cnct acceptConnection(int *serverSocket)
{
  struct clnt_to_pi_cnct clntPiConnection;
  clntPiConnection.clnt_sd =
      accept(*serverSocket, (struct sockaddr *)&clntPiConnection.clntAddr,
             malloc(sizeof(clntPiConnection.clntAddr)));
  if (clntPiConnection.clnt_sd < 0)
    dieWithSysMessage("error accept -1");
  printf("New client [%d] connected\n", clntPiConnection.clnt_sd);

  char *piAddress = getBestPi(); // identify the pi with least users
  roundTripTimePi(piAddress);
  // setup connection to pi
  printf("Setting up connection to %s on port %d\n", piAddress, PI_PORT);
  if ((clntPiConnection.pi_sd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    dieWithSysMessage("error on creating socket to pi");
  clntPiConnection.piAddr.sin_family = AF_INET;
  clntPiConnection.piAddr.sin_port = htons(PI_PORT);
  clntPiConnection.piAddr.sin_addr.s_addr = inet_addr(piAddress);
  memset(clntPiConnection.piAddr.sin_zero, '\0',
         sizeof(clntPiConnection.piAddr.sin_zero));
  // connect to Pi
  if (connect(clntPiConnection.pi_sd,
              (struct sockaddr *)&clntPiConnection.piAddr,
              sizeof(clntPiConnection.piAddr)) < 0)
    dieWithSysMessage("Error connect pi");
  return clntPiConnection;
}

void *transferDataToPi(void *threadArgs)
{
  struct clnt_to_pi_cnct connection = *(struct clnt_to_pi_cnct *)threadArgs;
  unsigned char buffer[BUFF] = {0};
  pthread_detach(pthread_self());
  ssize_t recBuff;
  do
  {
    recBuff = recv(connection.clnt_sd, &buffer, sizeof(buffer), 0);
    if (sendto(connection.pi_sd, &buffer, recBuff, 0,
               (struct sockaddr *)&connection.piAddr,
               sizeof(connection.piAddr)) == -1)
      dieWithSysMessage("error on sending to pi\n");
  } while (recBuff != 0);
  printf("client[%d] closed connection\n", connection.clnt_sd);
  exit(0); // end child process
}

void receiveDataFromPi(struct clnt_to_pi_cnct *threadArgs)
{
  struct clnt_to_pi_cnct connection = *threadArgs;
  unsigned char buffer[BUFF] = {0};
  pthread_detach(pthread_self());
  ssize_t recBuff;
  do
  {
    recBuff = recv(connection.pi_sd, &buffer, sizeof(buffer), 0);
    // forward to client
    if (sendto(connection.clnt_sd, &buffer, recBuff, 0,
               (struct sockaddr *)&connection.clntAddr,
               sizeof(connection.clntAddr)) == -1)
      dieWithSysMessage("error sending to client");
  } while (recBuff != 0);
  printf("pi[%d] closed connection\n", connection.pi_sd);
  exit(0); // end child process
}

void sendReceiveThread(struct clnt_to_pi_cnct passedConnection)
{
  pthread_t sendThread;
  if (pthread_create(&sendThread, NULL, transferDataToPi, &passedConnection) !=
      0)
    dieWithSysMessage("creating thread transferDataToPi failed");
  receiveDataFromPi(&passedConnection);
}

void processCleanup(unsigned int *childProcCount)
{
  if (*childProcCount > 5)
    printf("maximum of 5 similar connections reached \n");
  while (*childProcCount > 0)
  {                                                      // Clean up all zombies
    pid_t processID = waitpid((pid_t)-1, NULL, WNOHANG); // Non-blocking wait
    if (processID < 0)                                   // waitpid() error?
      dieWithSysMessage("waitpid() failed");
    if (processID == 0) // No zombie to wait on
      break;
    else
    {
      *childProcCount = *childProcCount - 1; // Cleaned up after a child ended
      printf("cleanup [%d] active processes remaining\n", *childProcCount);
    };
  }
}