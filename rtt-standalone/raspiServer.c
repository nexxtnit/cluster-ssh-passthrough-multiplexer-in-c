#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <sys/time.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

#define PORTREC 44480

struct rtt_vals
{
  //all times are in usec
  uint32_t clientSend_sec, clientSend_usec, raspiRec_sec,
      raspiRec_usec, raspiSend_sec, raspiSend_usec,
      clientRec_sec, clientRec_usec;
};

double printTimeDifference(uint32_t time1_sec, uint32_t time1_usec, uint32_t time0_sec, uint32_t time0_usec);

int main(int argc, char const *argv[])
{
  int port = PORTREC;
  if (argc == 2)
    port = atoi(argv[1]);
  printf("Server Started\n");

  struct sockaddr_in myaddr;            // our address
  struct sockaddr_in remoaddr;          // remote address
  socklen_t addrlen = sizeof(remoaddr); // length of address
  int recvlen, sd;                      // bytes received                              // filedescriptor our socket
  char *remote_address;
  struct rtt_vals rtt, emptyrtt_vals;
  struct timeval tv, tv_ioctl;

  // UDP_SOCKET
  if ((sd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
  {
    perror("cannot create socket\n");
    exit(EXIT_FAILURE);
  }

  // bind socker and specify port
  memset((char *)&myaddr, 0, sizeof(myaddr));
  myaddr.sin_family = AF_INET;
  myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  myaddr.sin_port = htons(port);

  if (bind(sd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0)
  {
    perror("socket: bind failed");
    exit(EXIT_FAILURE);
  }

  while (true)
  {
    recvlen =
        recvfrom(sd, &rtt, sizeof(rtt), 0, (struct sockaddr *)&remoaddr, &addrlen);

    if (recvlen > 0)
    {
      if (ioctl(sd, SIOCGSTAMP, &tv_ioctl) != 0)
        perror("ioctl failed, end programm");
      rtt.raspiRec_sec = tv_ioctl.tv_sec;   //time packet receive
      rtt.raspiRec_usec = tv_ioctl.tv_usec; //time packet receive

      double timeClusterToPi = printTimeDifference(rtt.raspiRec_sec,
                                                   rtt.raspiRec_usec,
                                                   rtt.clientSend_sec,
                                                   rtt.clientSend_usec);

      printf("transfer time cluster->pi: %f s \n", timeClusterToPi);

      gettimeofday(&tv, NULL);
      rtt.raspiSend_sec = tv.tv_sec;   //time packet send
      rtt.raspiSend_usec = tv.tv_usec; //time packet send
      sendto(sd, &rtt, sizeof(rtt), 0, (const struct sockaddr *)&remoaddr, addrlen);
      double timePiRecToSend = printTimeDifference(rtt.raspiSend_sec,
                                                   rtt.raspiSend_usec,
                                                   rtt.raspiRec_sec,
                                                   rtt.raspiRec_usec);

      printf("time on pi to return packet: %f s \n", timePiRecToSend);
    }
  }
}

double printTimeDifference(uint32_t time1_sec, uint32_t time1_usec, uint32_t time0_sec, uint32_t time0_usec)
{
  if (difftime(time1_sec, time0_sec) == 0)
  {
    double timediff_usec = difftime(time1_usec, time0_usec);
    if (timediff_usec < 0)
    {
      printf("ERR: Timedifference negative, receive-timestamp before send-timestamp\n");
      return -1;
    }
    else
    {
      return (timediff_usec * 0.000001);
    }
  }
  else if (difftime(time1_sec, time0_sec) > 0)
  {
    double timediff_sec = difftime(time1_sec, time0_sec);
    timediff_sec += (difftime(time1_usec, time0_usec) * 0.000001);
  }
  else
  {
    printf("ERR: Timedifference negative, receive-timestamp before send-timestamp\n");
    return -1;
  }
}
