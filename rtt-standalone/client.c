#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <sys/time.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>

#define PORTSEND 44480
#define DEFAULTADDR "127.0.0.1"

struct rtt_vals
{
  //all times are in usec
  uint32_t clientSend_sec, clientSend_usec, raspiRec_sec,
      raspiRec_usec, raspiSend_sec, raspiSend_usec,
      clientRec_sec, clientRec_usec;
};

double printTimeDifference(uint32_t time1_sec, uint32_t time1_usec, uint32_t time0_sec, uint32_t time0_usec);

int main(int argc, char *argv[])
{
  printf("Client Started\n");
  char send_addr[15] = DEFAULTADDR;
  int port = PORTSEND;
  if (argc == 2)
  {
    strcpy(send_addr, argv[1]);
  }
  else if (argc == 3)
  {
    strcpy(send_addr, argv[1]);
    port = atoi(argv[2]);
  }

  struct rtt_vals rtt;
  struct timeval tv, tv_ioctl;
  uint32_t clientReady_sec, clientReady_usec;
  int sd, n;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  socklen_t addr_size;

  if ((sd = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
  {
    perror("cannot create socket\n");
    exit(EXIT_FAILURE);
  }

  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(port);
  serverAddr.sin_addr.s_addr = inet_addr(send_addr);
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);
  addr_size = sizeof(serverAddr);

  gettimeofday(&tv, NULL); //get time right before client sends
  rtt.clientSend_sec = tv.tv_sec;
  rtt.clientSend_usec = tv.tv_usec;

  sendto(sd, &rtt, sizeof(rtt), 0, (struct sockaddr *)&serverAddr,
         addr_size);

  if (recv(sd, &rtt, sizeof(rtt), 0) < 0)
    perror("recv failed");
  if (ioctl(sd, SIOCGSTAMP, &tv_ioctl) < 0)
    perror("ioctl failed");
  gettimeofday(&tv, NULL); //get time client is ready
  rtt.clientRec_sec = tv_ioctl.tv_sec;
  rtt.clientRec_usec = tv_ioctl.tv_usec;
  clientReady_sec = tv.tv_sec;
  clientReady_usec = tv.tv_usec;
  double roundTripTime = printTimeDifference(rtt.clientRec_sec,
                                             rtt.clientRec_usec,
                                             rtt.clientSend_sec,
                                             rtt.clientSend_usec);
  printf("round trip time: %f s \n", roundTripTime);
  double networkToProgTime = printTimeDifference(clientReady_sec,
                                                 clientReady_usec,
                                                 rtt.clientRec_sec,
                                                 rtt.clientRec_usec);
  printf("transfer time networkadapter -> client: %f s \n", networkToProgTime);
  close(sd);
  return 0;
}
double printTimeDifference(uint32_t time1_sec, uint32_t time1_usec, uint32_t time0_sec, uint32_t time0_usec)
{
  if (difftime(time1_sec, time0_sec) == 0)
  {
    double timediff_usec = difftime(time1_usec, time0_usec);
    if (timediff_usec < 0)
    {
      printf("ERR: Timedifference negative, receive-timestamp before send-timestamp\n");
      return -1;
    }
    else
    {
      return (timediff_usec * 0.000001);
    }
  }
  else if (difftime(time1_sec, time0_sec) > 0)
  {
    double timediff_sec = difftime(time1_sec, time0_sec);
    timediff_sec += (difftime(time1_usec, time0_usec) * 0.000001);
  }
  else
  {
    printf("ERR: Timedifference negative, receive-timestamp before send-timestamp\n");
    return -1;
  }
}